<?php

    class Calculadora
    {
        //atrib
        public $nro1;
        public $nro2;

        //method
        public function Sumar()
        {
            //return $nro1 + $nro2;
            return $this->nro1 + $this->nro2;
        }
        public function Restar()
        {
            return $this->nro1 - $this->nro2;
        }
        public function Multiplicar()
        {
            return $this->nro1 * $this->nro2;
        }
       
        public function Dividir()
        {
            if($this ->nro2 == 0)
            {
                return "No se puede dividir entre 0";
            }
            else
            {
                return $this->nro1 / $this->nro2;
            }
        }
        private function Fact($nro)
        {
            if($nro == 0)
                return 1;
            else
                return $nro * $this->Fact($nro - 1);
        }

        public function Factorial()
        {
            return $this->Fact($this->nro1);
        }

        private function Potencia($nro1, $nro2)
        {
            if($nro2 == 0)
            {
                return 1;
            }
            else{
                return $nro1 * $this->Potencia($nro1 , $nro2 - 1);
            } 
        }

        public function Pot()
        {
            return $this->Potencia($this->nro1, $this->nro2);
        }


        public function Seno() {
            return (sin(deg2rad($this->nro1)));
        }
        public function Tangente() {
            return (tan(($this->nro1* pi()) / 180));
        }

        public function Coseno() {
            return (cos(($this->nro1* pi()) / 180));
        }

        public function Porcentaje() {
            return (($this->nro1/100)*$this->nro2);
        }

        public function Raaaiz() {
            return (sqrt($this->nro1));
        }

        public function Inversa() {
            return (1/$this->nro1);
        }
    }

?>
